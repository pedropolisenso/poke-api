import { configureStore, ThunkAction, Action } from '@reduxjs/toolkit';
import PokeListReducer from '../features/PokeList/PokeListSlice';
import PokeItemReducer from '../features/PokeItem/PokeItemSlice';
import PokeDetailsReducer from '../features/Details/DetailsSlice';
import PokeFavoritesReducer from '../features/Favorite/FavoriteSlice';
import PokeModalReducer from '../features/Modal/ModalSlice';
import PokeFilterReducer from '../features/Filter/FilterSclice';
import PokeItemsPerPageReducer from '../features/SelectItemPerPage/SelectItemsPerPageSlice';

export const store = configureStore({
  reducer: {
    details: PokeDetailsReducer,
    favorites: PokeFavoritesReducer,
    filter: PokeFilterReducer,
    itemsPerPage: PokeItemsPerPageReducer,
    modal: PokeModalReducer,
    pokes: PokeListReducer,
    selectedPoke: PokeItemReducer
  },
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;
