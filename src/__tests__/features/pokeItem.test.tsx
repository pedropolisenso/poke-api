import React from 'react';
import { render } from '@testing-library/react';
import PokeItem from '../../features/PokeItem/PokeItem';
import { Provider } from 'react-redux';
import { store } from '../../app/store';

describe('PokeItem component', () => {
  let wrapper: any;

  beforeEach(() => {
    const dataItem = {
        abilities: [],
        avatar: 'avatar',
        id: 123,
        name: 'picachu',
        types: []
    }

    wrapper = render(
      <Provider store={store}>
        <PokeItem dataItem={dataItem} />
      </Provider>
    );
  })

  it('renders whole component', () => {
    expect(wrapper).toMatchSnapshot();
  }); 
})