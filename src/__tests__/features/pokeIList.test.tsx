import React from 'react';
import { render } from '@testing-library/react';
import PokeList from '../../features/PokeList/PokeList';
import { Provider } from 'react-redux';
import { store } from '../../app/store';

describe('PokeList component', () => {
  let wrapper: any;

  beforeEach(() => {
    wrapper = render(
      <Provider store={store}>
        <PokeList />
      </Provider>
    );
  })

  it('renders whole component', () => {
    expect(wrapper).toMatchSnapshot();
  }); 
})