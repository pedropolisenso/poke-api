import React from 'react';
import { render } from '@testing-library/react';
import Filter from '../../features/Filter/Filter';
import { Provider } from 'react-redux';
import { store } from '../../app/store';

describe('Filter component', () => {
  let wrapper: any;

  beforeEach(() => {
    wrapper = render(
      <Provider store={store}>
        <Filter />
      </Provider>
    );
  })

  it('renders whole component', () => {
    expect(wrapper).toMatchSnapshot();
  }); 
})