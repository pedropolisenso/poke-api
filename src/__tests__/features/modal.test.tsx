import React from 'react';
import { render } from '@testing-library/react';
import Modal from '../../features/Modal/Modal';
import { Provider } from 'react-redux';
import { store } from '../../app/store';

describe('Modal component', () => {
  let wrapper: any;

  beforeEach(() => {
    wrapper = render(
      <Provider store={store}>
        <Modal />
      </Provider>
    );
  })

  it('renders whole component', () => {
    expect(wrapper).toMatchSnapshot();
  }); 
})