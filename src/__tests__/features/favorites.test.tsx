import React from 'react';
import { render } from '@testing-library/react';
import Favorite from '../../features/Favorite/Favorite';
import { Provider } from 'react-redux';
import { store } from '../../app/store';

describe('Favorite component', () => {
  let wrapper: any;

  beforeEach(() => {
    wrapper = render(
      <Provider store={store}>
        <Favorite id={10} />
      </Provider>
    );
  })

  it('renders whole component', () => {
    expect(wrapper).toMatchSnapshot();
  }); 
})