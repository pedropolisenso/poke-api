import React from 'react';
import { render } from '@testing-library/react';
import Search from '../../components/Search/Search';
import { Provider } from 'react-redux';
import { store } from '../../app/store';

describe('Search component', () => {
  let wrapper: any;

  beforeEach(() => {
    wrapper = render(
      <Provider store={store}>
        <Search />
      </Provider>
    );
  })

  it('renders whole component', () => {
    expect(wrapper).toMatchSnapshot();
  }); 
})