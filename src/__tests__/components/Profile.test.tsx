import React from 'react';
import { render } from '@testing-library/react';
import Profile from '../../components/Profile/Profile';
import { Provider } from 'react-redux';
import { store } from '../../app/store';

describe('Profile component', () => {
  let wrapper: any;

  beforeEach(() => {
    wrapper = render(
      <Provider store={store}>
        <Profile />
      </Provider>
    );
  })

  it('renders whole component', () => {
    expect(wrapper).toMatchSnapshot();
  }); 
})