import React from 'react';
import { render } from '@testing-library/react';
import Pagination from '../../components/Pagination/Pagination';
import { Provider } from 'react-redux';
import { store } from '../../app/store';

describe('Pagination component', () => {
  let wrapper: any;

  beforeEach(() => {
    const pages = {
        next: 'next',
        previous: null
    }

    wrapper = render(
      <Provider store={store}>
        <Pagination pages={pages} />
      </Provider>
    );
  })

  it('renders whole component', () => {
    expect(wrapper).toMatchSnapshot();
  }); 
})