import React, { useState, useEffect } from 'react'
import TextField from '@mui/material/TextField';
import IconButton from "@mui/material/IconButton";
import SearchIcon from '@mui/icons-material/Search';
import { useAppSelector, useAppDispatch } from '../../app/hooks'
import { fetchPokes } from '../../features/PokeList/PokeListSlice'

export const LiveSearch = () => {
  const dispatch = useAppDispatch()
  const [pokemonName, setPokemonName] = useState('')

  const handleChange = (event: any) => {
    setPokemonName(event.target.value as string);
  };

  const { itemsPerPage } = useAppSelector(state => state.itemsPerPage);

  const baseURL: string = "https://pokeapi.co/api/v2/pokemon";

  useEffect(() => {
    if (pokemonName === '') {
      dispatch(fetchPokes(`${baseURL}/?limit=${itemsPerPage}`))
    }
  }, [pokemonName])

  return (
    <>
      <TextField
        sx={{ marginTop: 2 }}
        size="small"
        label="Seach Pokemon"
        onChange={handleChange}
      />
      <IconButton
        type="submit"
        sx={{ p: '10px' }}
        aria-label="search"
        onClick={() => dispatch(fetchPokes(`${baseURL}/${pokemonName}`))}
      >
        <SearchIcon sx={{ marginTop: 2 }} />
      </IconButton>
    </>
  );
}

export default LiveSearch;
