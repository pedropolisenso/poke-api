import Stack from '@mui/material/Stack';
import Button from '@mui/material/Button';
import { useAppDispatch } from '../../app/hooks'

import { fetchPokes } from '../../features/PokeList/PokeListSlice'

type Pages = {
  next: string|null|undefined,
  previous: string|null|undefined
}

type Props = {
  pages: Pages;
}

export default function Pagination(props: Props) {
  const dispatch = useAppDispatch();

  return (
    <Stack spacing={5} direction="row" sx={{mt: 3}}>
      { props.pages.previous &&
        <Button variant="contained" onClick={() => dispatch(fetchPokes(props.pages.previous!))}>Previous</Button>
      }
      { props.pages.next &&
        <Button variant="contained" onClick={() => dispatch(fetchPokes(props.pages.next!))}>Next</Button>
      }
    </Stack>
  );
}