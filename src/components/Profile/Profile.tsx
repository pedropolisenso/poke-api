import Grid from '@mui/material/Grid';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import Chip from '@mui/material/Chip';
import Stack from '@mui/material/Stack';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import { useAppSelector } from '../../app/hooks'

export default function Profile() {
  const { poke } = useAppSelector(state => state.selectedPoke);

  return (
    <Grid item sx={{ p: 0 }}>
      {poke &&
        <>
          <CardMedia
            component="img"
            height="200px"
            image={poke?.avatar}
            alt={poke?.name}
            style={{objectFit: 'fill', marginBottom: '20px'}}
          />

          <Typography gutterBottom variant="h5" component="div" align="center">
            {poke?.name}
          </Typography>

          <Stack direction="row" spacing={1} justifyContent="center" sx={{mb: 4}}>
            {poke?.types.map((type: any, index: number) => {
              return ( <Chip key={index} label={type.type.name} /> )
            })}
          </Stack>

          <Grid container justifyContent="center">
          <Grid item lg={4}>
            <Typography gutterBottom variant="h6" component="div" align="left">
              Stats
            </Typography>
            <List>
            {poke?.stats.map((stat: any, index: number) => {
              return (
                <ListItem key={index} sx={{ pl: 0, pr: 0 }}>
                  <ListItemText primary={`${stat.stat.name}: ${stat.base_stat}`} />
                </ListItem>
              )
            })}
            </List>
          </Grid>
          <Grid item lg={4}>
            <Typography gutterBottom variant="h6" component="div" align="left">
              abilities
            </Typography>
            <List>
              {poke?.abilities.map((ability: any, index: number) => {
                return (
                  <ListItem key={index} sx={{ pl: 0 }}>
                    <ListItemText primary={ability.ability.name} />
                  </ListItem>
                )
              })}
            </List>
          </Grid>
          <Grid item lg={4}>
            <Typography gutterBottom variant="h6" component="div">
              Physical
            </Typography>
            <List>
            <ListItem sx={{ pl: 0 }}>
                <ListItemText primary={`Experience: ${poke?.base_experience}`} />
            </ListItem>
            <ListItem sx={{ pl: 0 }}>
                <ListItemText primary={`Height: ${poke?.height}`} />
            </ListItem>
            <ListItem sx={{ pl: 0 }}>
                <ListItemText primary={`Weight: ${poke?.weight}`} />
            </ListItem>
            </List>
          </Grid>
          </Grid>
        </>
      }
    </Grid>
  );
}
