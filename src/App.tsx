import Grid from '@mui/material/Grid';
import PokeList from './features/PokeList/PokeList'
import FilterSelect from './features/Filter/Filter';
import SelectItemPerPage from './features/SelectItemPerPage/SelectItemsPerPage';
import Profile from './components/Profile/Profile'
import BasicModal from './features/Modal/Modal'
import LiveSearch from './components/Search/Search'

import './App.css';

export const App = () => {
  return (
    <div className="App">
      <header>
        <Grid container spacing={2}>
          <Grid item>
            <h2>Poke API</h2>
          </Grid>
          <Grid item>
            <LiveSearch />
          </Grid>
          <Grid item>
            <FilterSelect />
          </Grid>
          <Grid item>
            <SelectItemPerPage />
          </Grid>
        </Grid>
      </header>

      <PokeList />
      <BasicModal>
        <Profile />
      </BasicModal>
    </div>
  );
}

export default App;
