export type Details = {
  count: number;
  next: string|null;
  previous: string|null;
  results: Results[];
}
  
export type Results = {
  name: string;
  url: string;
}

export type Poke = {
  abilities: any[]
  avatar: string;
  base_experience: number;
  height: number;
  id: number;
  name: string;
  order: number;
  stats: any[];
  types: PokeTypes[]
  weight: number;
  favorite?: boolean;
}

export type PokeTypes = {
  slot: number;
  type: {
    name: string;
    url: string;
  }
}

export type Pages = {
  next: string|null|undefined,
  previous: string|null|undefined
}