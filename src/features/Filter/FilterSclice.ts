import { createSlice } from '@reduxjs/toolkit';

type State = {
  filter: string;
}

type Action = {
  payload: any;
}

// SLICE
const filterSlice = createSlice({
  name: 'filter',
  initialState: {
    filter: 'all' as string,
  },
  reducers: {
    setFilter: (state: State, action: Action) => {
        state.filter = action.payload
    },
  }
})

  export const { setFilter } = filterSlice.actions
export default filterSlice.reducer
