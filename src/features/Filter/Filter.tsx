import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import InputLabel from '@mui/material/InputLabel';
import Box from '@mui/material/Box';
import FormControl from '@mui/material/FormControl';
import { useAppDispatch, useAppSelector } from '../../app/hooks'

import { setFilter } from './FilterSclice'

export const FilterSelect = () => {
  const { favorites } = useAppSelector(state => state.favorites);
  const { filter } = useAppSelector(state => state.filter);
  const hasFavorite: number = favorites?.length || 0

  const dispatch = useAppDispatch()
  const handleChange = (event: any) => {
    dispatch(setFilter(event.target.value))
  }
  return (
    <Box sx={{ minWidth: 120, marginTop: 2 }}>
      <FormControl fullWidth>
        <InputLabel id="demo-simple-select-label">All</InputLabel>
        <Select
          size="small"
          value={filter}
          label="pokemon"
          onChange={handleChange}
        >
          <MenuItem value="all">All Pokes</MenuItem>
          <MenuItem value="favorite">Captured Pokemons</MenuItem>
          {hasFavorite > 0 &&
            <MenuItem value="not-favorite">Not Captured</MenuItem>
          }
        </Select>
      </FormControl>
    </Box>
  );
}

export default FilterSelect;
