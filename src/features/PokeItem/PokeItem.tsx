import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import { CardActionArea } from '@mui/material';
import Grid from '@mui/material/Grid';
import { useAppDispatch } from '../../app/hooks';
import { selectPoke } from './PokeItemSlice';
import { dispatchModal } from '../Modal/ModalSlice'
import Favorite from '../Favorite/Favorite';
import { PokeTypes } from '../../types';

type Props = {
  abilities: any[]
  avatar: string;
  id: number;
  name: string;
  types: PokeTypes[]
  favorite?: boolean;
}

export default function PokeCard({ dataItem }: { dataItem: Props }) {
  const dispatch = useAppDispatch()
  const { avatar, name, id } = dataItem
  
  return (
    <Grid item>
      <Card>
        <Favorite id={id} />
        <CardActionArea onClick={() => {
          dispatch(selectPoke(dataItem))
          dispatch(dispatchModal(true))
        }}>
          <CardMedia
            component="img"
            height="100"
            image={avatar}
            alt={name}
            style={{objectFit: 'fill'}}
          />
          <CardContent>
            <Typography gutterBottom variant="h5" component="div" align="center">
              {name}
            </Typography>
          </CardContent>
        </CardActionArea>
      </Card>
    </Grid>
  );
}

