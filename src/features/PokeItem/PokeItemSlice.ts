import { createSlice } from '@reduxjs/toolkit';
import { Poke } from '../../types';

// LOCAL TYPE
type State = {
  poke: Poke|null;
}

type Action = {
  payload: any;
}

// SLICE
export const selectedPokeSlice = createSlice({
    name: 'pokeItem',
    initialState: {
      poke: null as Poke|null
    },
    reducers: {
      selectPoke: (state: State, action: Action) => {
        state.poke = action.payload
      }
    }
  })

  export const { selectPoke } = selectedPokeSlice.actions
  export default selectedPokeSlice.reducer
