import { createSlice } from '@reduxjs/toolkit';

// TYPES
type Details = {
    count: number;
    next: string|null;
    previous: string|null;
    results: Results[];
}

type Results = {
    name: string;
    url: string;
}

// SLICE
export const setPokeDetailsSlice = createSlice({
    name: 'setDetails',
    initialState: {
        details: null as Details|null
    },
    reducers: {
      setDetails: (state: any, action: any) => {
        state.details = action.payload
      }
    }
  })

  export const { setDetails } = setPokeDetailsSlice.actions
  export default setPokeDetailsSlice.reducer
