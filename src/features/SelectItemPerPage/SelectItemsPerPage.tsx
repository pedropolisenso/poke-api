import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import InputLabel from '@mui/material/InputLabel';
import Box from '@mui/material/Box';
import FormControl from '@mui/material/FormControl';
import { useAppDispatch, useAppSelector } from '../../app/hooks'

import { setItemsPerPage } from './SelectItemsPerPageSlice'

export const SelectItemsPerPage = () => {
  const { itemsPerPage } = useAppSelector(state => state.itemsPerPage);
  const dispatch = useAppDispatch();

  const handleChange = (event: any) => {
    dispatch(setItemsPerPage(event.target.value))
  }

  return (
    <Box sx={{ minWidth: 120, marginTop: 2 }}>
      <FormControl fullWidth>
        <InputLabel id="demo-simple-select-label">Items</InputLabel>
        <Select
          size="small"
          value={itemsPerPage}
          label="pages"
          onChange={handleChange}
        >
          <MenuItem value="20">20</MenuItem>
          <MenuItem value="40">40</MenuItem>
          <MenuItem value="60">60</MenuItem>
          <MenuItem value="80">80</MenuItem>
          <MenuItem value="100">100</MenuItem>
        </Select>
      </FormControl>
    </Box>
  );
}

export default SelectItemsPerPage;
