import { createSlice } from '@reduxjs/toolkit';

// LOCAL TYPE

type State = {
  itemsPerPage: string;
}

type Action = {
  payload: any;
}

// SLICE
const SelectItemsPerPageSlice = createSlice({
  name: 'selectItemsPerPage',
  initialState: {
    itemsPerPage: "20" as string,
  },
  reducers: {
    setItemsPerPage: (state: State, action: Action) => {
        state.itemsPerPage = action.payload
    },
  }
})

export const { setItemsPerPage } = SelectItemsPerPageSlice.actions
export default SelectItemsPerPageSlice.reducer
