import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import { setDetails } from '../Details/DetailsSlice';
import { Details, Poke } from '../../types';

// LOCAL TYPE
type State = {
  list: Poke[];
  status: string;
}

type Action = {
  payload: any;
}

// ACTION
export const fetchPokes = createAsyncThunk(
  'pokes/fetchPokes',
  async (url: string, {dispatch}) => {
    return fetch(url)
      .then(response => response.json())
      .then((details: any) => {
        if (details && !details.results) {
          return [{
            ...details,
            avatar: details.sprites.other.dream_world.front_default
          }]
        }
        const { results, next, previous, count }: Details = details
        dispatch(setDetails({next, previous, count}))

        return Promise.all(Array.from({ length: results.length }, (_, i) =>
          fetch(results[i].url)
            .then(response => response.json())
            .then(({ abilities, base_experience, height, id, name, order, sprites, stats, types, weight }) => ({
              abilities,
              avatar: sprites.other.dream_world.front_default,
              base_experience,
              height,
              id,
              name,
              order,
              stats,
              types,
              weight
            }))
            .catch((error) => error)
          ))
      })
  }
)

// SLICE
const pokesSlice = createSlice({
    name: 'pokes',
    initialState: {
      list: [] as Poke[],
      status: 'loading' as string
    },
    reducers: {
      setFavorite: (state: State, action: Action) => {
        state.list.map((item: Poke) => {
          if (item.id === action.payload) {
            item.favorite = true
          }
        })
      },
      unSetFavorite: (state: State, action: Action) => {
        state.list.map((item: Poke) => {
          if (item.id === action.payload) {
            item.favorite = false
          }
        })
      }
    },
    extraReducers: (builder) => {
      builder.addCase(fetchPokes.pending, (state: State) => {
        state.status = 'loading'
      })
      builder.addCase(fetchPokes.fulfilled, (state: State, { payload }: { payload: any }) => {
        state.list = payload
        state.status = 'done'
      })
      builder.addCase(fetchPokes.rejected, (state: State) => {
        state.status = 'failed'
      })
    },
  })

  export const { setFavorite, unSetFavorite } = pokesSlice.actions
export default pokesSlice.reducer
