import React, { useEffect } from 'react';
import Grid from '@mui/material/Grid';
import PokeItem from '../PokeItem/PokeItem'
import { useAppSelector, useAppDispatch } from '../../app/hooks'
import { fetchPokes } from './PokeListSlice'
import Typography from '@mui/material/Typography';
import Pagination from '../../components/Pagination/Pagination'
import CircularProgress from '@mui/material/CircularProgress';
import { Pages, Poke } from '../../types';

const PokeList = () => {
  const dispatch = useAppDispatch()
  const { list, status } = useAppSelector(state => state.pokes);
  const { details } = useAppSelector(state => state.details);
  const { favorites } = useAppSelector(state => state.favorites);
  const { filter } = useAppSelector(state => state.filter);
  const { itemsPerPage } = useAppSelector(state => state.itemsPerPage);

  const pages: Pages = {
    next: details?.next,
    previous: details?.previous
  }

  useEffect(() => {
      dispatch(fetchPokes(`https://pokeapi.co/api/v2/pokemon?limit=${itemsPerPage}`))
  }, [dispatch, itemsPerPage])
  
  if (status === 'loading') {
    return (
      <Grid container justifyContent="center">
        <CircularProgress />
      </Grid>
    )
  }

  if (status === 'failed') {
    return (
      <Grid container justifyContent="center">
        <Typography gutterBottom variant="h6" component="p" sx={{ ml: 5}}>
          Pokemon not found
        </Typography>
      </Grid>
    )
  }

  return (
    <>
      <Typography gutterBottom variant="h6" component="p" sx={{ ml: 5}}>
        Pokemon List
      </Typography>
      <Grid container rowSpacing={1} columnSpacing={{ xs: 1, sm: 2, md: 3, lg: 2 }} sx={{ py: 3, px: 5 }}>
        {filter === 'all' && list?.map(((item: Poke) =>(
          <Grid key={`all-${item.id}`} item lg={2} md={4} sm={6} xs={12}>
            <PokeItem key={item.id} dataItem={item} />
          </Grid>
        )))}
        {filter === 'favorite' && list?.filter((item: Poke) => favorites?.includes(item.id))?.map((item =>(
          <Grid key={`favorite-${item.id}`} item lg={2} md={4} sm={6} xs={12}>
            <PokeItem key={item.id} dataItem={item} />
          </Grid>
        )))}
        {filter === 'not-favorite' && list?.filter((item: Poke) => !favorites?.includes(item.id))?.map((item =>(
          <Grid key={`not-favorite-${item.id}`} item lg={2} md={4} sm={6} xs={12}>
            <PokeItem key={item.id} dataItem={item} />
          </Grid>
        )))}
        {favorites?.length === 0 && filter === 'favorite' &&
          <Typography gutterBottom variant="h4" component="h4" sx={{ ml: 5}}>
            No pokemons Captured
          </Typography>
        }
      </Grid>
      <Grid sx={{ ml: 5, mb: 2 }}>
        {list!.length > 1 && <Pagination pages={pages} />}
      </Grid>
    </>
  )
}

export default PokeList
