import { useAppSelector, useAppDispatch } from '../../app/hooks'
import { setFavorite, unSetFavorite } from '../Favorite/FavoriteSlice'
import IconButton from '@mui/material/IconButton';
import Box from '@mui/material/Box';

type Props = {
  id: number;
}

export default function Favorite(props: Props) {
  const dispatch = useAppDispatch()
  const { favorites } = useAppSelector(state => state.favorites);

  return (
    <>
      {favorites?.includes(props.id) &&
        <IconButton onClick={() => dispatch(unSetFavorite(props.id))} aria-label="add to favorites">
          <Box
            component="img"
            sx={{ width: 25 }}
            alt="catch pokemon"
            src="./assets/img/captired.png"
          />
        </IconButton>
      }
      {!favorites?.includes(props.id) &&
        <IconButton onClick={() => dispatch(setFavorite(props.id))} aria-label="add to favorites">
          <Box
            component="img"
            sx={{ width: 25 }}
            alt="catch pokemon"
            src="./assets/img/captire.png"
          />
        </IconButton>
      }
    </>
  );
}

