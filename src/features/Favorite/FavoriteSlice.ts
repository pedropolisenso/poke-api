import { createSlice } from '@reduxjs/toolkit';

// LOCAL TYPE
type State = {
  favorites: number[]|null
}

type Action = {
  payload: any;
}

// SLICE
const favoritesSlice = createSlice({
  name: 'favorites',
  initialState: {
    favorites: [] as number[]|null,
  },
  reducers: {
    setFavorite: (state: State, action: Action) => {
      state.favorites!.push(action.payload)
    },
    unSetFavorite: (state: State, action: Action) => {
      const index = state.favorites!.findIndex((favorite: number) => favorite === action.payload)
      state.favorites!.splice(index, 1)
    }
  }
})

export const { setFavorite, unSetFavorite } = favoritesSlice.actions
export default favoritesSlice.reducer
