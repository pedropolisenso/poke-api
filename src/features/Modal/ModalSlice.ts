import { createSlice } from '@reduxjs/toolkit';

// LOCAL TYPE
type State = {
  openModal: boolean;
}

type Action = {
  payload: any;
}

// SLICE
export const modalPokeSlice = createSlice({
    name: 'modal',
    initialState: {
        openModal: false as boolean

    },
    reducers: {
      dispatchModal: (state: State, action: Action) => {
        state.openModal = action.payload
      }
    }
  })

  export const { dispatchModal } = modalPokeSlice.actions
  export default modalPokeSlice.reducer
