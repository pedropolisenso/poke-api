## Pokemon App

This is a simple app using [Pokemon API](https://pokeapi.co/)

#### Steps

- Clone repository
- cd `repository`
- run `npm install`


### Run app

> npm start


### Run tests

> npm tests